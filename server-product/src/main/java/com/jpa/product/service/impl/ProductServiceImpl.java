package com.jpa.product.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jpa.product.dao.ProductDao;
import com.jpa.product.entity.Product;
import com.jpa.product.service.ProductService;

import java.util.List;

/**
 * @author: MR.LIU
 * @description:
 * @date: 2020/5/29
 * @time: 22:41
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;

    @Override
    public Product findById(Long id) {
        return productDao.findById(id).get();
    }

    @Override
    public void save(Product product) {
         productDao.save(product);
    }

    @Override
    public void update(Product product) {
        productDao.save(product);
    }

    @Override
    public void delete(Long id) {
        productDao.deleteById(id);
    }

    @Override
    public List<Product> findByName(String name) {
        return productDao.findByNameMatch(name);
    }
}
