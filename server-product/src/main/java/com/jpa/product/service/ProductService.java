package com.jpa.product.service;


import com.jpa.product.entity.Product;

import java.util.List;

/**
 * @author: MR.LIU
 * @description: 接口层
 * @date: 2020/5/29
 * @time: 22:37
 */
public interface ProductService {
    /**
     * 根据id查询
     * @param id
     * @return
     */
    Product findById(Long id);

    /**
     * 保存
     * @param product
     * @return
     */
    void save(Product product);

    /***
     * 修改
     * @param product
     * @return
     */
    void update(Product product);

    /**
     * 删除
     * @param id
     * @return
     */
    void delete(Long id);

    /**
     * 模糊查询
     * @param name
     * @return
     */
    List<Product> findByName(String name);
}
