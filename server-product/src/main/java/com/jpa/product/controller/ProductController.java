package com.jpa.product.controller;

import com.jpa.product.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.jpa.product.service.ProductService;

import java.util.List;

/**
 * @author: MR.LIU
 * @description:
 * @date: 2020/5/29
 * @time: 22:47
 */
@RestController
@RequestMapping(value = "/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping(value = "/byId/{id}")
    public Product getProduct(@PathVariable Long id) {
        return productService.findById(id);
    }

    @PostMapping(value = "/save")
    public String saveProduct(@RequestBody Product product){
        productService.save(product);
        return "保存成功";
    }

    @GetMapping(value = "/byName/{name}")
    public List<Product> getProductByName(@PathVariable String name) {
        return productService.findByName(name);
    }

}
