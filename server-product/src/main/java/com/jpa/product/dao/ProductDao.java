package com.jpa.product.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.jpa.product.entity.Product;

import java.util.List;

/**
 * @author: MR.LIU
 * @description: 数据层
 * @date: 2020/5/29
 * @time: 22:34
 */
public interface ProductDao extends JpaRepository<Product,Long>, JpaSpecificationExecutor<Product> {
    @Query(value = "select * from product where product_name like concat('%',:name,'%')",nativeQuery = true)
    List<Product> findByNameMatch(@Param("name") String name);
}
