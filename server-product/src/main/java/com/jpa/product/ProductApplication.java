package com.jpa.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: MR.LIU
 * @description:
 * @date: 2020/5/29
 * @time: 22:52
 */
@SpringBootApplication
@EntityScan(value = "com.jpa.product.entity")
//开启注册
//@EnableEurekaClient
@EnableDiscoveryClient
public class ProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class,args);
    }
}
