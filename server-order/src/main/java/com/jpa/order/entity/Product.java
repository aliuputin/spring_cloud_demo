package com.jpa.order.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author: MR.LIU
 * @description:
 * @date: 2020/5/29
 * @time: 22:17
 */
@Data
public class Product {

    private Long id;

    private String productName;

    private BigDecimal price;

    private String description;

    private Integer status;

    private String caption;

    private Integer stock;
}
