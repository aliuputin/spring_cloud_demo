package com.jpa.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author: MR.LIU
 * @description:
 * @date: 2020/5/30
 * @time: 15:55
 */
@SpringBootApplication
@EntityScan(value = "com.jpa.order.entity")
@EnableDiscoveryClient
public class OrderApplication {
    @Bean
    public RestTemplate restTemplate(){
       return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class,args);
    }
}
