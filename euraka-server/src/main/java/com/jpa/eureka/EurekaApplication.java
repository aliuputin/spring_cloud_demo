package com.jpa.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author: MR.LIU
 * @description:
 * @date: 2020/5/30
 * @time: 17:09
 */
@SpringBootApplication
//开启EurekaServer
@EnableEurekaServer
@EnableDiscoveryClient
public class EurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaApplication.class,args);
    }
}
